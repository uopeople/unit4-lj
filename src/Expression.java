import java.util.List;
import java.util.TreeSet;

public class Expression {
	private TreeSet<Integer> left;
	private String operation;
	private TreeSet<Integer> right;	
	
	public Expression(List<Integer> left, String operation, List<Integer> right) {
		this.left = new TreeSet<Integer>(left);
		this.operation = operation;
		this.right = new TreeSet<Integer>(right);
	}

	/**
	 * Calculates the result of the expression
	 * @return a TreeSet representing the result
	 * @throws Exception on invalid operation
	 */
	public TreeSet<Integer> calculate() throws Exception {
		TreeSet<Integer> setA = new TreeSet<Integer>(this.left); // Creates a copy so the original is not manipulated
		TreeSet<Integer> setB = this.right;
		
		switch (operation) {
			case "+":
				// Union
				setA.addAll(setB);
				break;
			case "-":
				// Intersection
				setA.removeAll(setB);
				break;
			case "*":
				// Difference
				setA.retainAll(setB);
				break;
			default:
				throw new Exception("Invalid operation");
		}
				
		return setA;
	}

	@Override
	public String toString() {
		return this.left.toString() + " " + this.operation + " " + this.right.toString();
	}
}
