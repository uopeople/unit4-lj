import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputParser {
	// Match the following pattern: [****]+|-|*[****]
	private static final Pattern ExpressionPattern = Pattern.compile("^(\\[(.*)\\])([+*-])(\\[(.*)\\])$");
	
	/**
	 * Parses the input into an Expression
	 * @param input - Expression String
	 * @return an Expression
	 * @throws Exception on any parsing error
	 */
	public Expression parse(String input) throws Exception {
		if (input == null || input.length() == 0) throw new Exception("Empty input");
		input = input.replace(" ", ""); // Remove all spaces
		
		Matcher matcher = ExpressionPattern.matcher(input);
		if (!matcher.find()) throw new Exception("Invalid input");
		
		String left = matcher.group(1); // First array
		String operation = matcher.group(3); // Operation
		String right = matcher.group(4); // Second array
		
		return new Expression(parseArray(left), operation, parseArray(right));
	}

	/**
	 * Parses a an array string into a list of integers
	 * @param arrayString - the array string
	 * @return a List of integers
	 * @throws Exception on empty array string
	 */
	private List<Integer> parseArray(String arrayString) throws Exception {
		if (arrayString == null || arrayString.length() == 0) throw new Exception("Empty array");
		arrayString = arrayString.replace("[", "").replace("]", ""); // Remove brackets
								 
		String[] integers = arrayString.split(",");
		List<Integer> result = new ArrayList<Integer>(integers.length);
		
		for (String integer : integers) {
			result.add(Integer.parseInt(integer));
		}
		
		return result;
	}
}
