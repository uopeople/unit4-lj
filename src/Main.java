import java.util.Scanner;
import java.util.TreeSet;

public class Main {
	private static final Scanner Scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("CS 1103 - AY2017-T1, Unit 4, Learning Journal");

		System.out.println("Please provide expressions to calculate. When done enter 'X' to quit.");
		System.out.println();
		
		InputParser parser = new InputParser(); // Initialize parser
		
		while (true) { // Continue until user quits
			try {
				// Get expressions from user
				String input = readLine("Calculate expression:");
				
				if (input.equals("X")) {
					// User wants to quit
					System.out.println();
					System.out.println("Thank you, bye...");
					return;
				}
				
				Expression expression = parser.parse(input);
				TreeSet<Integer> result = expression.calculate();
				
				System.out.println(expression + " = " + result);
				
			} catch (Exception ex) {
				System.out.println("Invalid input");
			}

			System.out.println(); // Add another spaced line
		}
	}
	
	/**
	 * Prompts a message to the user and return user's input
	 * @param prompt - The prompt to the user
	 * @return String of the user's input
	 */
	private static String readLine(String prompt) {
		System.out.print(prompt + " ");
		return Scanner.nextLine();
	}
}
